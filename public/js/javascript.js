function loadUp(){
    document.getElementById("wrapper").style.opacity = "1";
    
    setTimeout(function(){
        document.getElementById("top_row").style.opacity = "1";
        document.getElementById("wrapbio").style.opacity = "1";
    }, 1000);
    
    setTimeout(function(){
        document.getElementById("banners").style.opacity = "1";
        document.getElementById("footers").style.opacity = "1";
    }, 1500);
}

function toggle_elements(){
    var banners = document.getElementById("banners");
    var mainnav = document.getElementById('navbar');
    var footer = document.getElementById('footer');
    var footerlinks = document.getElementById('footer_links');

    var profile = document.getElementById('profile_page');
    var skills = document.getElementById('skills_page');
    var experience = document.getElementById('experience_page');

    var profile_link = document.getElementById("profile_link");
    var skills_link = document.getElementById("skills_link");
    var experience_link = document.getElementById("experience_link");

    footer.style.opacity = "0";
    banners.style.opacity = "0";
    profile.style.opacity = "0";
    skills.style.opacity = "0";
    experience.style.opacity = "0";

    profile_link.style.color = "black";
    skills_link.style.color = "black";
    experience_link.style.color = "black";

    setTimeout(function(){
        footer.style.display = "none";
        banners.style.display = "none";
        skills.style.display = "none";
        experience.style.display = "none";
        profile.style.display = "none";

        mainnav.style.display = "block";
        mainnav.style.opacity = "1"; 
    }, 500);
        
    return;
}

function toggle_page(page){
    toggle_elements();
    
    var profile = document.getElementById('profile_page');
    var skills = document.getElementById('skills_page');
    var experience = document.getElementById('experience_page');

    var banners = document.getElementById("banners");
    var mainnav = document.getElementById('navbar');
    var footer = document.getElementById('footer');

    var profile_link = document.getElementById("profile_link");
    var skills_link = document.getElementById("skills_link");
    var experience_link = document.getElementById("experience_link");
    
    
    if(page == "profile"){
        setTimeout(function(){
        profile.style.display = "block";
        profile_link.style.color = "red";
        }, 600);
        setTimeout(function(){
    
            profile.style.opacity = "1";
                
            }, 700);
        
    }
    else if(page == 'skills'){
        setTimeout(function(){

        skills.style.display = "block";
        skills_link.style.color = "red";
        }, 600);
        setTimeout(function(){
            skills.style.opacity = "1";
                
            }, 700);
    }
    else if(page == "experience"){
        setTimeout(function(){
        experience.style.display = "block";
        }, 600);
        setTimeout(function(){
            experience.style.opacity = "1";
            experience_link.style.color = "red";
            }, 700);
    }
    else if(page == 'hobbies'){
        setTimeout(function(){
            hobbies.style.display = "block";  
            
        }, 600);
        
        
        setTimeout(function(){
            hobbies.style.opacity = "1";
                
            }, 700);
    }
    else if(page == 'home'){
        setTimeout(function(){
            mainnav.style.opacity = "0";
            
        }, 100);

        setTimeout(function(){
            mainnav.style.opacity = "0";
            banners.style.display = "block";   
            mainnav.style.display = "none";
            banners.style.opacity = "1";
            footer.style.opacity = "1";
            footer.style.display = "block";
        }, 500);

    }
}


function modalShow(target){
    // Get the modal
    var modal = document.getElementById(target);

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var modalImg = document.getElementById(target + "_Img".toString());
    modal.style.display = "block";

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close");

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
    modal.style.display = "none";
    } 
}

function modalClose(target){
    var modal = document.getElementById(target);
    modal.style.display = "none";
}