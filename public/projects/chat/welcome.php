<?php 
    //Sessiomuuttujat sekä tarkistetaan onko aktiivinen sessio käynnissä
    include "../../../connect.php";
    include "../../../session_check.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Character settings -->
    <meta charset="utf-8">
      
    <!-- Viewport settings -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Mobile first -->
      
    <!-- Title image -->
      
    <!-- External file links -->
      
    <link rel="stylesheet" type="text/css" href="css/main.css"/>           <!-- Stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/fontawesome-all.css"/> <!-- Styles and fonts for media links -->
      
    <!--Custom fonts -->
    <link href="https://fonts.googleapis.com/css?family=Alata|Alatsi|Open+Sans|Roboto+Condensed|Source+Sans+Pro&display=swap" rel="stylesheet"> 
      
    <script src="js/javascript.js"></script>                                <!-- JavaScript file for animations -->
      
    <!-- Text showing up in the top bar -->
     <title>IRC-CHAT</title>
      
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
      
    <!-- Bootstrap links -->  
      
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> 

</head>
<body>
<div class="container-fluid" style="padding: 0;">
    <div class="row"> 
        <div class="col-sm-12" id="header">
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="padding: initial 35px;">
            <p class="nav-title">
                Welcome, <?php echo $_SESSION['kayttajanimi'] . "(" . $_SESSION['oikeudet'] . ")"; ?> 
                <br> Last login: <?php echo $_SESSION['lastlogin'];?> 
            </p>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
                </li>
                <?php
                if($_SESSION['oikeudet'] == "admin")
                {echo "<li class='nav-item'><a class='nav-link' href='addchannel.php'>New server</a></li>";}?>
                <li class="nav-item">
                <a class="nav-link" href="scripts/logout.php">Logout</a>
                </li>
            </div>
        </div>
    </nav>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h3 class="primary-title">Active users</h3> 
            <div class="user_list">
                <?php
                $sql3 = "select * from logindata where active = 'Yes'";
                $run3 = mysqli_query($con, $sql3) or die ("Kysely epäonnistui5");
                while($row = mysqli_fetch_array($run3)){
                    if($row['username'] == 'admin')
                    {
                        echo "<p class='active_user'>". ucfirst($row['username']) . "<img src='images/svg/star.svg'/ class='admin_star'>" . "</p>";
                    }
                    else{
                        echo "<p class='active_user'>". ucfirst($row['username']) . "</p>";
                    }
                }
                ?>

            </div>
        </div>       <!--Aktiiviset käyttäjät -->
    </div>
    <div class="row">
            <div class="col-sm-12">
                <div class="serverlist">
                    <h3 class="primary-title">Channels</h3> 
                    <table border="2">
                            <tr>
                                <th>Name</th>
                                <th>Access</th>
                            </tr>
                        <?php             
                        $sql = "SELECT * FROM channeldata ORDER BY channel_ID";
                        $run = mysqli_query($con, $sql) or die ("Kysely epäonnistui55");  
                        while($row = mysqli_fetch_array($run)){?>
                        <?php $hash= $row['hash'];?>
                        <tr>
                            <td><?php echo "<form action='chat.php' method='post'><button type='submit' name='hashkoodi' value='$hash'>" .$row['channel_ID']. "</button></form>";?></td>  
                            <td><?php echo $row['private'];?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
    </div>
<footer class="row">
    <div class="col-sm-12">
      <!-- Copyright -->
      <div class="footer-copyright text-center py-3">© 2019 Build and design by 
        <a href="https://sakutupala.com">Saku Tupala </a>
      </div>
      <!-- Copyright -->
    </div>
</footer> 
    
</div>
</body>
</html>