<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Character settings -->
    <meta charset="utf-8">
      
    <!-- Viewport settings -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Mobile first -->
      
    <!-- Title image -->
      
    <!-- External file links -->
      
    <link rel="stylesheet" type="text/css" href="css/main.css"/>           <!-- Stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/fontawesome-all.css"/> <!-- Styles and fonts for media links -->
      
    <!--Custom fonts -->
    <link href="https://fonts.googleapis.com/css?family=Merriweather|Open+Sans|Slabo+27px|Source+Sans+Pro" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Aleo" rel="stylesheet">   
      
    <script src="js/javascript.js"></script>                                <!-- JavaScript file for animations -->
      
    <!-- Text showing up in the top bar -->
     <title>IRC-CHAT</title>
      
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
      
    <!-- Bootstrap links -->  
      
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> 
</head>
<body>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
        <h2 class="primary-title">Create a new account</h2>
        <form class="form" action="scripts/confirm.php" method="post">
        <p class="form-title">Register</p>
          <input type="text" placeholder="username" name="kayttajanimi" required/>
          <input type="password" placeholder="password" name="salasana" required/>
          <input type="text" placeholder="email" name="sahkoposti"/>
          <button type="submit">Register</button>
          <p class="form-bottom-notice">Already user? <a href="index.php">Sign in</a></p>
        </form>
        
      </div> 
  </div> 
</div>
</body>
 </html>