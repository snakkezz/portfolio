<?php 
    //Sessiomuuttujat sekä tarkistetaan onko aktiivinen sessio käynnissä
    include "../../../connect.php";
    session_start();
    if(isset($_SESSION['kayttajanimi'])){ //Jos käyttäjäsessio on aktiivinen, asetetaan muuttujat
        $user = $_SESSION['kayttajanimi'];
        $sql = "select priviledges from userdata where username = '$user'";
        $run = mysqli_query($con, $sql) or die ("Kysely epäonnistui1");  
        while($row = mysqli_fetch_array($run)){
            $_SESSION['oikeudet'] = $row['priviledges'];
        }
    }
    else {  //Jos sessio ei ole aktiivinen, palautetaan etusivulle
        header("location:index.php");
    }

    if($_SESSION['oikeudet'] != "admin"){
        //header("location:welcome.php");
    }
$logID = $_SESSION['logID'];
$pvm = date("Y-m-d H:i:s");   
$sql7 = "UPDATE logindata SET active ='No', logouttime='$pvm'  WHERE username = '$user' AND loginNumber = '$logID'";
    

$time = $_SERVER['REQUEST_TIME'];

/**
* for a 30 minute timeout, specified in seconds
*/
$timeout_duration = 1800;

/**
* Here we look for the user's LAST_ACTIVITY timestamp. If
* it's set and indicates our $timeout_duration has passed,
* blow away any previous $_SESSION data and start a new one.
*/
if (isset($_SESSION['LAST_ACTIVITY']) && 
   ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
    $run7 = mysqli_query($con, $sql7) or die ("Kysely epäonnistui2");
    session_unset();
    session_destroy();
    session_start();
    $_SESSION['message'] = "Your session was expired";
    header("location:index.php");
}

/**
* Finally, update LAST_ACTIVITY so that our timeout
* is based on it and not the user's login time.
*/
$_SESSION['LAST_ACTIVITY'] = $time;

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Character settings -->
    <meta charset="utf-8">
      
    <!-- Viewport settings -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Mobile first -->
      
    <!-- Title image -->
      
    <!-- External file links -->
      
    <link rel="stylesheet" type="text/css" href="css/main.css"/>           <!-- Stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/fontawesome-all.css"/> <!-- Styles and fonts for media links -->
      
    <!--Custom fonts -->
    <link href="https://fonts.googleapis.com/css?family=Merriweather|Open+Sans|Slabo+27px|Source+Sans+Pro" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Aleo" rel="stylesheet">   
      
    <script src="js/javascript.js"></script>                                <!-- JavaScript file for animations -->
      
    <!-- Text showing up in the top bar -->
     <title>IRC-CHAT</title>
      
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
      
    <!-- Bootstrap links -->  
      
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> 
</head>
<body>
<div class="container-fluid" style="padding: 0;">
<div class="row"> 
        <div class="col-sm-12">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <p class="nav-title">
            Welcome, <?php echo $_SESSION['kayttajanimi'] . "(" . $_SESSION['oikeudet'] . ")"; ?> 
            <br>
            Last login: <?php echo $_SESSION['lastlogin'];?> 
            </p>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="welcome.php">Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="scripts/logout.php">Logout</a>
                </li>
            </div>
        </div>
    </nav>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="new-channel-form">
                <form class="form" action="scripts/add_channel_database.php" method="post">
                <p class="form-title">Create a new server</p>
                    <label for="channelname">Give your channel a name</label>
                    <input type="text" placeholder="Channel name" name="channelID" id="channelname" required> <br>
                    Protection:<br>
                    <input type="radio" name="protection" <?php if (isset($adminonly) && $admin-only=="Admin-only") {echo "checked";}?> value="admins" id="admin-only">
                    <label for="admin-only" class="radio-label">Admin only</label>
                    
                    <input type="radio" name="protection" <?php if (isset($adminonly) && $admin-only=="Yes") echo "checked";?> value="password" onclick="insertPW()" id="password-only">
                    <label for="password-only" class="radio-label">Password</label>

                    <input type="radio" name="protection" <?php if (isset($adminonly) && $admin-only=="No") echo "checked";?> value="none" id="none">
                    <label for="none" class="radio-label" active>None</label>
                    
                    <br><button type="submit">Add</button>
                    <br><br><a href="welcome.php" class="link-button">Return</a>
                </form>
            </div>
        </div>
    </div> 
</div>  
<script>
function insertPW(){
    if(document.getElementById('yespw').checked){
    document.getElementById("pass").style.display = 'block';
    }
    else {
    document.getElementById("pass").style.display = 'none';
    }
}       
</script>
</body>