<?php 
    //Sessiomuuttujat sekä tarkistetaan onko aktiivinen sessio käynnissä
    include "../../../connect.php";
    session_start();
    if(isset($_SESSION['kayttajanimi'])){ //Jos käyttäjäsessio on aktiivinen, asetetaan muuttujat
        $user = $_SESSION['kayttajanimi'];
        $sql = "select priviledges from userdata where username = '$user'";
        $run = mysqli_query($con, $sql) or die ("Kysely epäonnistui 1");  
        while($row = mysqli_fetch_array($run)){
            $_SESSION['oikeudet'] = $row['priviledges'];
        }
    }
    else {  //Jos sessio ei ole aktiivinen, palautetaan etusivulle
        header("location:index.php");
    }

    //Asetetaan session timeout
    $logID = $_SESSION['logID'];
    $pvm = date("Y-m-d H:i:s");   
    $sql7 = "UPDATE logindata SET active ='No', logouttime='$pvm'  WHERE username = '$user' AND loginNumber = '$logID'";

    $time = $_SERVER['REQUEST_TIME'];

    /* for a 30 minute timeout, specified in seconds*/
    $timeout_duration = 1800;
    /**
    * Here we look for the user's LAST_ACTIVITY timestamp. If
    * it's set and indicates our $timeout_duration has passed,
    * blow away any previous $_SESSION data and start a new one.
    */
    if (isset($_SESSION['LAST_ACTIVITY']) && 
       ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
        $run7 = mysqli_query($con, $sql7) or die ("Kysely epäonnistui2");
        session_unset();
        session_destroy();
        session_start();
        $_SESSION['message'] = "Your session was expired";
        header("location:index.php");
}
    /**
    * Finally, update LAST_ACTIVITY so that our timeout
    * is based on it and not the user's login time.
    */
    $_SESSION['LAST_ACTIVITY'] = $time;
    //-----------------------------------------------------
    //Katsotaan minkä kanavan käyttäjä valitsi ja haetaan viestit sen mukaan
    $error = "username/password incorrect";
    if( isset($_POST['hashkoodi']) )
        {
            $koodi = $_POST['hashkoodi'];
            $_SESSION['hash'] = $koodi;
            $sql9 = "SELECT channel_ID from channeldata WHERE hash = '$koodi'";
            $run9 = mysqli_query($con, $sql9) or die ("Kysely epäonnistui3");
            while($row = mysqli_fetch_array($run9)){
                       $channel = $row['channel_ID'];
                }
            
        }
    else {
            $koodi = $_SESSION['hash'];
            $sql9 = "SELECT channel_ID from channeldata WHERE channel_ID = '$koodi'";
            $run9 = mysqli_query($con, $sql9) or die ($koodi);
            while($row = mysqli_fetch_array($run9)){
                       $channel = $row['name'];
                }        
        }
    $sql5 = "UPDATE logindata SET activechannel='$koodi' WHERE username='$user' AND active = 'Yes'";
    $run5 = mysqli_query($con, $sql5) or die ("Kysely epäonnistui5");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Character settings -->
    <meta charset="utf-8">
      
    <!-- Viewport settings -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Mobile first -->
      
    <!-- Title image -->
      
    <!-- External file links -->
      
    <link rel="stylesheet" type="text/css" href="css/main.css"/>           <!-- Stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/fontawesome-all.css"/> <!-- Styles and fonts for media links -->
      
    <!--Custom fonts -->
    <link href="https://fonts.googleapis.com/css?family=Alata|Alatsi|Open+Sans|Roboto+Condensed|Source+Sans+Pro&display=swap" rel="stylesheet"> 
      
    <script src="js/javascript.js"></script>                                <!-- JavaScript file for animations -->
      
    <!-- Text showing up in the top bar -->
     <title>Chat room</title>
      
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
      
    <!-- Bootstrap links -->  
      
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> 

</head>
<body>
<div class="container-fluid" style="padding: 0;">
<div class="row"> 
        <div class="col-sm-12" id="header">
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="padding: initial 35px;">
            <p class="nav-title">
                Welcome, <?php echo $_SESSION['kayttajanimi'] . "(" . $_SESSION['oikeudet'] . ")"; ?> 
                <br> Last login: <?php echo $_SESSION['lastlogin'];?> 
            </p>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="welcome.php">Home<span class="sr-only">(current)</span></a>
                </li>
                <?php
                if($_SESSION['oikeudet'] == "admin")
                {echo "<li class='nav-item'><a class='nav-link' href='addchannel.php'>New server</a></li>";}?>
                <li class="nav-item">
                    <a class="nav-link" href="scripts/logout.php">Logout</a>
                </li>
            </div>
        </div>
    </nav>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="room_info">
                <?php
                $pvm = date("d.m.Y H:i");
                echo "<p><span id='koodi'>You are in " . $channel . "</span></p>"; 
                echo "<p>" . "Today is ". $pvm . "</p>";?>
            </div>   <!-- Näyttää kanavan tiedot -->

            <h3 class="primary-title" style="margin: 0;">Users<hr></h3>
            <div class="user_list" style="margin: 1% auto; padding: 1px">
                <?php
                    $sql3 = "SELECT * from logindata where activechannel = '$koodi' AND active = 'Yes'";
                    $run3 = mysqli_query($con, $sql3) or die ("Kysely epäonnistui7");

                    while($row = mysqli_fetch_array($run3)){
                        if($row['username'] == 'admin')
                        {
                            echo "<p class='active_user'>". ucfirst($row['username']) . "<img src='images/svg/star.svg'/ class='admin_star'>" . "</p>";
                        }
                        else{
                            echo "<p class='active_user'>". ucfirst($row['username']) . "</p>";
                        }
                    }
                    ?>
            </div>  <!--Aktiiviset käyttäjät näkyvät tässä -->
            <form class="chat_box" id="chat">
                <div class="message_box" id="messagebox">
                <?php
                    $sql = "SELECT date,message,username FROM messagedata WHERE hash = '$koodi'";
                    $run = mysqli_query($con, $sql) or die ("Kysely epäonnistui6"); 
                    while($row = mysqli_fetch_array($run))
                    { 
                        echo "[" .$row['date'] ."] ";
                        echo "<strong>" .$row['username'] .": </strong>";
                        echo $row['message']. "<br>";      
                    } 
                ?>
                </div>
                <input type="text" placeholder="Write your message..." name="usermsg" id="usermsg" autocomplete="off"/>
                <button name="submitmsg" type="submit" id="submitmsg" class="user_btn">Send</button>
            </form>
        </div>
    </div>
    <!--Main content-->
     
    <script> 
        var element = document.getElementById("messagebox");
        element.scrollTop = element.scrollHeight;

        function updateScroll(){
            var element = document.getElementById("messagebox");
            element.scrollTop = element.scrollHeight;
        }
        // Variable to hold request
        var request;
        // Bind to the submit event of our form
        $("#chat").submit(function(event){

            // Prevent default posting of form - put here to work in case of errors
            event.preventDefault();

            // Abort any pending request
            if (request) {
                request.abort();
            }
            // setup some local variables
            var $form = $(this);

            // Let's select and cache all the fields
            var $inputs = $form.find("input, select, button, textarea");

            // Let's disable the inputs for the duration of the Ajax request.
            // Note: we disable elements AFTER the form data has been serialized.
            // Disabled form elements will not be serialized.
            $inputs.prop("disabled", true);

            // Fire off the request to /form.php
            request = $.ajax({
                url: "scripts/usersend.php",
                type: "post",
                data: {empData:$inputs.val()},
                success: function(data)
                {
                     $('#messagebox').html(data);
                }
            });

            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR, data){
                // Log a message to the console
                console.log("Hooray, it worked!");
                updateScroll();
                $('#usermsg').val('');

            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown){
                // Log the error to the console
                console.error(
                    "The following error occurred: "+
                    textStatus, errorThrown
                );
            });

            // Callback handler that will be called regardless
            // if the request failed or succeeded
            request.always(function () {
                // Reenable the inputs
                $inputs.prop("disabled", false);
            });

        });
                //Let's create key listener, so message can be sent by pressing enter    
                var input = document.getElementById("usermsg");
                input.addEventListener("keyup", function(event) {
                    event.preventDefault();
                    if (event.keyCode === 13) {
                        document.getElementById("submitmsg").click();
                    }
            });
</script>           <!--Ajax-skripti viestien lähettämiseen-->
</div>
</body>
</html>