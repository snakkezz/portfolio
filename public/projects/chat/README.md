# Live-chat development

This is a source code project for my school assigment at my university JAMK.

The project is a chat-system mostly written in PHP. System includes user-system and MySQL database.
Back-end was prioritised due to time limit so current goal is to visually enchance the site.

Chat and back-end has some vulnerabilities that need fixing, so for example, login data is not encrypted. 
Therefor, do not use real information on registations, until further notice.

You may login using test users:  
http://sakutupala.com/projects/chat/
Testi / 1234  
admin / admin  


## Current features

**Main features**
* Login and registration
* Users with different priviledges (Admin/basic)   
* Users can see a list of different chat channels
* Chat channels can be password protected or be Admin-only channels
* Users can join channels within their priviledges
* Users can see previous messages at the chat
* Users can send new messages
* Users can see active users on chat  
* Admins can create new channels and choose settings for them (Name, password)
* Admins can join any channel without password

**Additional features**
* Users can see their previous login

  
## Planned features

**Critical**
* Password encryption
* Admins can delete messages and channels
* Messages can be send without refreshing, but fetching messages doens't work without refreshing

**Non-prioritized**
* User profile
* Test commit




